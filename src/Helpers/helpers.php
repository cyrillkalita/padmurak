<?php

use Padmurak\Helpers\Strings;

/*
|--------------------------------------------------------------------------
| Helpers
|--------------------------------------------------------------------------
|
| There are High Level Functions, simplifying some mundane tasks
*/

if (!function_exists('model')) {
    /**
     * Resolve standard Model
     *
     * @param  string|null  $name
     * @param  string|null  $namespace
     * @return boolean
     */
    function model($model = null, $namespace = 'App\Models')
    {
        return app("{$namespace}\\{$model}");
    }
}

if (!function_exists('the_phone')) {
    /**
     * Convert string to a phone number
     *
     * @param  string $value
     * @param  string $delimiter
     * @return string
     */
    function the_phone($value, $delimiter = '-')
    {
        return Strings::thePhone($value, $delimiter);
    }
}
