<?php

namespace Padmurak\Helpers;

class Strings
{
    /**
     * Format the string to a phone number
     *
     * @param  string $phoneNumber
     * @param  string $delimiter
     * @return string
     */
    public static function thePhone($phoneNumber, $delimiter = '-')
    {
        $phoneNumber = preg_replace('/[^0-9]/', '', $phoneNumber);

        if (strlen($phoneNumber) > 10) {
            # we have the long phone
            $countryCode = substr($phoneNumber, 0, strlen($phoneNumber)-10);
            $areaCode    = substr($phoneNumber, -10, 3);
            $nextThree   = substr($phoneNumber, -7, 3);
            $lastFour    = substr($phoneNumber, -4, 4);
            $phoneNumber = '+'.$countryCode.' ('.$areaCode.') '.$nextThree.$delimiter.$lastFour;
        } elseif (strlen($phoneNumber) == 10) {
            # we have the full phone
            $areaCode    = substr($phoneNumber, 0, 3);
            $nextThree   = substr($phoneNumber, 3, 3);
            $lastFour    = substr($phoneNumber, 6, 4);
            $phoneNumber = '('.$areaCode.') '.$nextThree.$delimiter.$lastFour;
        } elseif (strlen($phoneNumber) == 7) {
            # we have the short phone
            $nextThree   = substr($phoneNumber, 0, 3);
            $lastFour    = substr($phoneNumber, 3, 4);
            $phoneNumber = $nextThree.$delimiter.$lastFour;
        }
        return $phoneNumber;
    }
}
