<?php

namespace Padmurak\Providers;

use Illuminate\Support\ServiceProvider;

abstract class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Shared Namespace for the Composers
     *
     * @var string
     */
    protected $namespace;

    /**
     * List Composers to use
     *
     * @see https://laravel.com/docs/5.3/views#view-composers
     * @var array
     */
    protected $composers = [];

    /**
     * Boot Singletons
     *
     * @return Void
     */
    public function boot()
    {
        # Bind composers
        $this->app->view->composers($this->getComposers());
    }

    /**
     * Register Composers
     *
     * @return Void
     */
    public function register()
    {
        # Void
    }

    /**
     * Return composers
     *
     * @return Array
     */
    protected function getComposers()
    {
        # split keys from values
        list($composers, $views)   = array_divide($this->composers);
        # apply callback to prepend Namespace
        return array_combine($this->prependNamespace($composers), $views);
    }

    /**
     * Prepend the shared namespace
     *
     * @param  array $composers
     * @return array
     */
    protected function prependNamespace($composers)
    {
        return array_map(function ($key) {
            return empty($this->namespace) ? $key : "{$this->namespace}\\$key";
        }, $composers);
    }
}
