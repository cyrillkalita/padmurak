<?php

namespace Padmurak\Composers;

use Carbon\Carbon;

abstract class ViewComposer
{
    /**
     * Dropdown variable name associated with class
     *
     * @var array
     */
    protected $name;

    /**
     * Share the variable into all views that request it
     *
     * @param  \Illuminate\View\View $view
     * @return Void
     */
    abstract public function compose($view);

    /**
     * Resolve User
     *
     * @return User | null
     */
    protected function user()
    {
        return auth()->user();
    }

    /**
     * Current moment of time
     *
     * @return Carbon\Carbon
     */
    protected function now()
    {
        return Carbon::now();
    }
}
