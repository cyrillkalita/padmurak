<?php

namespace Padmurak\Eloquent;

use Padmurak\Contracts\StatefulModel;
use Illuminate\Database\Eloquent\Model as BaseModel;

abstract class Model extends BaseModel
{
    use AbstractModelAttributes;
    use AbstractModelScopes;
    use ObserverableBootTrait;

    /**
     * Return only a given number of attributes and properties
     *
     * @param  string|array $attribute
     * @return array
     */
    public function only($attribute, $default = null)
    {
        foreach ((array)$attribute as $key) {
            array_set($only, $key, object_get($this, $key, $default));
        }
        return array_collapse($only);
    }

    /**
     * Ensure there is at least once value in array
     *
     * @param  \Arrayable|array|int $values
     * @return array
     */
    protected function forceProtectArray($values, $impossibleId = 0)
    {
        if ($values instanceof Arrayable) {
            $values = $values->toArray();
        }
        return array_prepend((array) $values, $impossibleId);
    }

    /**
     * True if the Field in question is dirty, was null and not is not null
     *
     * @param  string $field
     * @return boolean
     */
    public function isDirtyFromNull($field)
    {
        return $this->isDirty($field)
           and is_null($this->getOriginal($field))
           and !is_null($this->getAttribute($field));
    }
}
