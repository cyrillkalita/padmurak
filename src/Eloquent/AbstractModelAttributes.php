<?php

namespace Padmurak\Eloquent;

use Illuminate\Database\Eloquent\Model;

trait AbstractModelAttributes
{
    /**
     * Convert model key to its MD5 hash
     *
     * @return string
     */
    public function getMd5Attribute()
    {
        return md5($this->getKey());
    }

    /**
     * Encrypt the model morph class for label decryption
     *
     * @return string
     */
    public function getContextRefAttribute()
    {
        return encrypt([
            'context_type' => $this->getMorphClass(),
            'context_id' => $this->getKey(),
        ]);
    }
}
