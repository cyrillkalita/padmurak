<?php

namespace Padmurak\Eloquent;

use Illuminate\Database\Eloquent\Relations\Pivot as EloquentPivot;

class Pivot extends EloquentPivot
{
    use ObserverableBootTrait;
    use AbstractModelAttributes;
}
