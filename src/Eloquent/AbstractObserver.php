<?php

namespace Padmurak\Eloquent;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

abstract class AbstractObserver
{
    /**
     * Set the base created by field
     *
     * @var string
     */
    protected $creatorField = 'created_by_id';

    /**
     * Set Current User as Created By
     *
     * @param  Model $model
     * @return void
     */
    protected function setCreatedBy(Model $model)
    {
        $model->setAttribute($this->creatorField, $this->getCreatedBy($model));
    }

    /**
     * Resolve the creator for the model
     *
     * @param  Model $model
     * @return int
     */
    protected function getCreatedBy(Model $model)
    {
        return $model->getAttribute($this->creatorField) ?: $this->userId();
    }

    /**
     * Get current User ID
     *
     * @return int
     */
    protected function userId()
    {
        return object_get($this->user(), 'id', $this->defaultUserId());
    }

    /**
     * Resolve Current User
     *
     * @return User|null
     */
    protected function user()
    {
        return auth()->user();
    }

    /**
     * Return Default User Id
     *
     * @return int
     */
    protected function defaultUserId()
    {
        return config('padmurak.defaults.default_created_by_id');
    }

    /**
     * Return Now for the models that need it
     *
     * @return Carbon\Carbon
     */
    protected function now()
    {
        return Carbon::now();
    }

    /**
     * Clean up and return numbers only
     *
     * @param  mixed $value
     * @return int
     */
    protected function numbersOnly($value)
    {
        return preg_replace('/[^0-9]/', '', $value);
    }

    /**
     * Set nullable value into the field
     *
     * @param Model $model
     * @param string $field
     */
    protected function setNullableField(Model $model, $field)
    {
        $value = $model->getAttribute($field);
        # Compare the Value to empty string
        if ($value === '') {
            $value = null;
        }
        # set the attribute
        $model->setAttribute($field, $value);
    }
}
