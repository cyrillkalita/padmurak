<?php

namespace Padmurak\Eloquent;

use Padmurak\Eloquent\Model;
use Padmurak\Eloquent\AbstractObserver as Observer;

class StatefulModelObserver extends Observer
{
    /**
     * Listen to the "saved" event.
     *
     * @param  Model $model
     * @return void
     */
    public function saved(Model $model)
    {
        if($this->stateChange($model)){
            $this->saveStateChange($model);
        }
    }

    /**
     * True if the state of the model is changed
     *
     * @param  Model  $model
     * @return boolean
     */
    protected function stateChange(Model $model)
    {
        return $model->isDirty($model->getStatusFieldName());
    }

    /**
     * Record the state change
     *
     * @param  Model  $model
     * @return Model
     */
    protected function saveStateChange(Model $model)
    {
        return $model->histories()->create([
            'from_status_id' => $this->state($model, true),
            'to_status_id' => $this->state($model),
            'changes' => $this->changes($model),
        ]);
    }

    /**
     * Resolve the state attribute
     *
     * @param  Model   $model
     * @param  boolean $original
     * @return int | null
     */
    protected function state(Model $model, $original = false)
    {
        $method = $original ? 'getOriginal' : 'getAttribute';

        return $model->$method($model->getStatusFieldName());
    }

    /**
     * What are the changes, recorded in json
     *
     * @param  Model  $model
     * @return string
     */
    protected function changes(Model $model)
    {
        return json_encode($model->getDirty());
    }
}
