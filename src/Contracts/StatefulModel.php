<?php

namespace Padmurak\Contracts;

interface StatefulModel
{
    /**
     * Stateful Model must know what field stores state
     *
     * @return string
     */
    public function getStatusFieldName();

    /**
     * Any Stateful model needs to implement history
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function histories();
}
