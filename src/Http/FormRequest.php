<?php

namespace Padmurak\Http;

use Illuminate\Foundation\Http\FormRequest as IlluminateFormRequest;

abstract class FormRequest extends IlluminateFormRequest
{
    /**
     * List validation Rules
     *
     * @var array
     */
    protected $rules = [];

    /**
     * List authorization rules
     *
     * @var array
     */
    protected $permissions = [];

    /**
     * List sanitizing methods
     *
     * @var array
     */
    protected $sanitizers = [];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        # run sanitizers
        $this->sanitize();
        # return rules
        return $this->rules;
    }

    /**
     * Sanitize
     *
     * @return void
     */
    protected function sanitize()
    {
        foreach ($this->sanitizers as $sanitizer) {
            $this->$sanitizer();
        }
    }
}
