<?php

namespace Padmurak\Http;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

trait HandlesUploads
{

    /**
     * Where shall we store the files?
     *
     * @example app|private|uploads
     * @var string
     */
    protected $storagePath = 'uploads';

    /**
     * Filesystem to store the files in
     *
     * @example local|public
     * @var string
     */
    protected $disk = 'local';

    /**
     * Field in the Request to hold the files
     *
     * @var string
     */
    protected $uploadableField = 'file';

    /**
     * Check if the file is in request and then - handle one by one
     *
     * @param  Request $request
     * @return Collection
     */
    protected function handleUpload(Request $request)
    {
        return $this->handleFile($request->{$this->uploadableField});
    }


    /**
     * Upload the file with slugging to a given path
     *
     * @param UploadedFile $file
     * @return string
     */
    protected function handleFile(UploadedFile $file, $createNew = true, $storagetype = null)
    {
        # Validate file and if the uploaded file is not valid, return immediatelly
        if (!$file->isValid()) {
            return false;
        }
        # make safe fileName
        $fileName = $this->makeSafeUploadName($file);
        # Prepare the Document array
        $document = [
            'label'         => $file->getClientOriginalName(),
            'storage'       => $this->disk($storagetype),
            'original_name' => $file->getClientOriginalName(),
            'file_name'     => $fileName,
            'mime_type'     => $file->getClientMimeType(),
            'size'          => $file->getClientSize(),
            'uri'           => $this->getUri($fileName, $storagetype),
        ];
        # Move the files to proper location
        $file->storeAs($this->storagePath($storagetype), $fileName, $this->disk($storagetype));
        # Create a document or return array
        return $createNew ? $this->documentModel()->create($document) : $document;
    }

    /**
     * Resolve storage path
     *
     * @param  mixed $storagetype
     * @return string
     */
    protected function storagePath($storagetype = null)
    {
        return $this->storagePath = $storagetype ?: $this->storagePath;
    }

    /**
     * Resolve Disk
     *
     * @param  mixed $storagetype
     * @return string
     */
    protected function disk($storagetype = null)
    {
        return $this->disk =  $storagetype ?: $this->disk;
    }

    /**
     * Compile safe Name
     *
     * @param  UploadedFile $file
     * @return string
     */
    protected function makeSafeUploadName(UploadedFile $file)
    {
        $name = implode('_', [md5(microtime().$file->path()), $file->getClientOriginalName()]);

        return $this->sanitizeFileName($name);
    }

    /**
     * Clean up the file name
     *
     * @param  String $string
     * @return String
     */
    protected function sanitizeFileName($string)
    {
        return  $string;
    }

    /**
     * We shall save all uploads in one spot.. right?
     *
     * @param  string $name
     * @return string
     */
    protected function getUri($name, $storagetype = null)
    {
        $storagePath = $this->storagePath($storagetype);

        return storage_path("app/{$storagePath}/$name");
    }

    /**
     * Resolve the model from the config
     *
     * @return Illuminate\Database\Eloquent\Model
     */
    protected function documentModel()
    {
        return app()->make(config('binary-cats.documents.model'));
    }
}
