<?php

namespace Padmurak\Transformer;

use Carbon\Carbon;

trait StandardAttributes
{
    /**
     * Current Date
     *
     * @return string
     */
    protected function getCurrentDateAttribute()
    {
        return Carbon::today()->format('F j, Y');
    }

    /**
     * Currency
     *
     * @return string
     */
    protected function getUsdAttribute()
    {
        return '$';
    }
}
