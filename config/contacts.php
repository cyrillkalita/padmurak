<?php

return [

    /*
    |--------------------------------------------------------------------------
    | IT Contact
    |--------------------------------------------------------------------------
    |
    |
    */

    'it' => [
        'name' => env('CONTACT_IT_FROM', config('app.name')),
        'email' => env('CONTACT_IT_EMAIL'),
        'phone' => env('CONTACT_IT_PHONE'),
    ]
];
