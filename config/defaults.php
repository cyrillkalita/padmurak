<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default CreatedBy ID
    |--------------------------------------------------------------------------
    |
    | Use this value to substitute missing user ID in creating models
    */

    'default_created_by_id' => env('BC_DEFAULT_USER', 0),
];
