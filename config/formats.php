<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Formats to default to
    |--------------------------------------------------------------------------
    |
    |
    */

    /**
     * Short Date format
     *
     */
    'short_date' => 'M j, Y',

    /**
     * Long Date format
     *
     */
    'long_date' => 'F j, Y',

    /**
     * Full Date format
     *
     */
    'full_date' => 'l, F j, Y',

    /**
     * Time format
     *
     */
    'time' => 'g:i A',
];
