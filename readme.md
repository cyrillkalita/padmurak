## About Padmurak

This is a collection of base primitived that I commonly use for Laravel projects I build. This is based on [Laravel](https://laravel.com) at least version 5.5, although I imagine there is nothing in the functionality that requires this specific version.

## Models
Extend `Padmurak\Eloquent\Model` to use the automatic Observer registration and some opinionated look-ups that are omitted in the standard version.
Any model extending `Padmurak\Eloquent\Model` need to register an Observer (located in `..\Observers` namespace relative to the location of the model).

## Http
`Padmurak\Http\Controller` ensures standard features of any Controller:
- [x] AuthorizesRequests
- [x] DispatchesJobs
- [x] ValidatesRequests

`Padmurak\Http\FormRequest` allows for teh seamless sanitizing, rules and ACL / RBAC permission check.
Note: You still have to implement RBAC  or ACL though.

`Padmurak\Http\HandlesUploads` allows for seamless uploading functionality I am used to.
Note: this is sucject to breaking funcitonality and most possibly would be deprecated in the future.

## License

This is obviously open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
